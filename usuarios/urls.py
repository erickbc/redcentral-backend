
from django.conf.urls import url,include
from django.contrib import admin

from usuarios.views import *

urlpatterns = [
	url(r'^$', UserPerfilList.as_view()),
	url(r'^(?P<pk>[0-9]+)/$', UserDetail.as_view(),name='user-detail'),
	url(r'^create/$', createUser,name='user-create'),
	url(r'^assignToken/$', assignToken,name='assignToken'),
	url(r'^delete/(?P<pk>[0-9]+)/$', deleteUser,name='user-delete'),
	url(r'^permisos/(?P<pk>[0-9]+)/$', UserPermisos.as_view(),name='delete-user'),
	url(r'^update/(?P<pk>[0-9]+)/$', updateUser,name='user-permiso'),
]


