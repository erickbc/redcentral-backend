from rest_framework import serializers
from usuarios.models import *
from django.contrib.auth.models import User
class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username','first_name','last_name')

class OrganizacionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Organizacion
        fields = ('id','descripcion')

class PermisosSerializer(serializers.ModelSerializer):
	class Meta:
		model = Permiso
		#fields = ('id','general_lectura','reportes_tickets',)

class UserPerfilUsuario(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    organizacion = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='descripcion'
     )
    class Meta:
        model = PerfilUsuario
        fields = ( 'user','tipoUsuario','organizacion','statusUser')