# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
"""
class Categoria(models.Model):
    descripcion = models.CharField(max_length=30, unique = True)
    status = models.BooleanField(default=True)

class Permiso(models.Model):
	categoria = models.ForeignKey(Categoria,related_name='category')
	descripcion = models.CharField(max_length=30, unique = True)
	status = models.BooleanField(default=True)

class Permisos(models.Model):
	permiso = models.ForeignKey(Permiso,related_name='permiso_usuario')
	status = models.BooleanField(default=True)

class UsuarioPermisos(models.Model):
	user = models.ForeignKey(User)
	permisos = models.ManyToManyField(Permisos,related_name='permisos_usuarios')
"""
class Permiso(models.Model):
	user = models.ForeignKey(User)
	general_lectura = models.BooleanField(default=False)
	general_escritura = models.BooleanField(default=False)
	reportes_tickets = models.BooleanField(default=False)
	reportes_alarmas = models.BooleanField(default=False)
	reportes_performance = models.BooleanField(default=False)
	reportes_slas = models.BooleanField(default=False)
	configuracion_usuarios = models.BooleanField(default=False)
	personalizacion_logotipo = models.BooleanField(default=False)
	tickets_consultar = models.BooleanField(default=False)
	tickets_registrar = models.BooleanField(default=False)


class Organizacion(models.Model):
    descripcion = models.CharField(max_length=30, unique = True)
    status = models.BooleanField(default=True)

class PerfilUsuario(models.Model):
	TYPE_PERFIL = (
		('2001', 'Administrador'),
		('2002', 'Usuario'),
	)
	TYPE_STATUS = (
		('S001', 'BLOQUEADO'),
		('S002', 'ACTIVO'),
		('S003', 'INACTIVO')
	)
	
	user = models.ForeignKey(User)
	tipoUsuario  = models.CharField(max_length=1, choices=TYPE_PERFIL)
	organizacion =  models.ForeignKey(Organizacion)
	tokenDevice =  models.CharField(max_length=500,null=True)
	uuidDevice =  models.CharField(max_length=500,null=True)
	contadorIntents = models.IntegerField(default=0)
	statusUser  = models.CharField(max_length=1, choices=TYPE_STATUS,default='S002')

