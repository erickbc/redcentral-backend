# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-05 04:29
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Organizacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=30, unique=True)),
                ('status', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='PerfilUsuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipoUsuario', models.CharField(choices=[('2001', 'Administrador'), ('2002', 'Usuario')], max_length=1)),
                ('organizacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='usuarios.Organizacion')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Permiso',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('general_lectura', models.BooleanField(default=False)),
                ('general_escritura', models.BooleanField(default=False)),
                ('reportes_tickets', models.BooleanField(default=False)),
                ('reportes_performance', models.BooleanField(default=False)),
                ('reportes_slas', models.BooleanField(default=False)),
                ('configuracion_usuarios', models.BooleanField(default=False)),
                ('personalizacion_logotipo', models.BooleanField(default=False)),
                ('tickets_consultar', models.BooleanField(default=False)),
                ('tickets_registrar', models.BooleanField(default=False)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
