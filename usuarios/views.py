# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth import authenticate
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.status import HTTP_401_UNAUTHORIZED
from rest_framework.authtoken.models import Token
from usuarios.models import *
from rest_framework import generics
from usuarios.serializer import UserPerfilUsuario,PermisosSerializer,UserSerializer
from django.contrib.auth.hashers import check_password
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
import json
import requests
from django.conf import settings
from django.db import IntegrityError
from rest_framework.permissions import IsAuthenticated

# Create your views here.
@api_view(["POST"])
def assignToken(request):
	try:
		username = request.data.get("username")
		token = request.data.get("tokenId")
		userLogin = User.objects.get(username = username)
		profile = PerfilUsuario.objects.get(user = userLogin)
		profile.tokenDevice = token
		profile.save()
		return Response({"status": True})
	except Exception, e:
		return Response({"status": False})

def descryptPassword(password):
	print "==========Descrypt Password=========="
	querystring = {"password":password}
	response = requests.request("POST",settings.URL_ENCRYPT, json=querystring)
	d = json.loads(response.text)
	return d["password"]
	
# Create your views here.
@api_view(["POST"])
@csrf_exempt
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")
    uuid = request.data.get("uuid")
    try:
    	password = descryptPassword(password)
    except Exception as e:
    	return Response({"error": "E005","description":"LOGIN FAILED"}, status=HTTP_401_UNAUTHORIZED)



    tokenDevice = request.data.get("tokenDevice")

    user = authenticate(username=username, password=password)
    if not user:
    	exist = User.objects.filter(username=username).exists()
    	if exist:
    		print "EXIST"
    		user = User.objects.get(username=username)
    		perfil = PerfilUsuario.objects.get(user=user)
    		contador = perfil.contadorIntents + 1
    		if contador >= 3:
    			perfil.contadorIntents = 3
    			perfil.statusUser = 'S001'
    			perfil.save()
    			#user.is_active = False
    			user.save()
    			return Response({"error": "E004","description":"USER BLOCKED"}, status=HTTP_401_UNAUTHORIZED)
    		else:
    			perfil.contadorIntents = contador
    			perfil.save()
    			return Response({"error": "E001","description":"LOGIN FAILED"}, status=HTTP_401_UNAUTHORIZED)

        else:
        	print "EXIST FALSE"
        	return Response({"error": "E001","description":"LOGIN FAILED"}, status=HTTP_401_UNAUTHORIZED)


    token, _ = Token.objects.get_or_create(user=user)
    perfil = PerfilUsuario.objects.get(user = user)
    if user.username != "erick":
    	if perfil.uuidDevice != uuid:

    		perfil = PerfilUsuario.objects.get(user=user)
    		contador = perfil.contadorIntents + 1
    		if contador == 3:
    			perfil.contadorIntents = 3
    			perfil.statusUser = 'S001'
    			perfil.save()
    			#user.is_active = False
    			user.save()
    			return Response({"error": "E004","description":"USER BLOCKED"}, status=HTTP_401_UNAUTHORIZED)
    		else:
    			perfil.contadorIntents = contador

    			perfil.save()
        		return Response({"error": "E002","description":"DEVICE NOT REGISTERED"}, status=HTTP_401_UNAUTHORIZED)

    contador = perfil.contadorIntents
    if contador >= 3:
    	return Response({"error": "E004","description":"USER BLOCKED"}, status=HTTP_401_UNAUTHORIZED)

    perfil.tokenDevice = tokenDevice
    perfil.save()
    permisos = Permiso.objects.get(user = user)
    active = user.is_active
    if active:
    	perfil = PerfilUsuario.objects.get(user=user)
    	perfil.contadorIntents = 0
    	perfil.save()

    	return Response({"token": token.key,"tipo":perfil.tipoUsuario,"status":True,"idUser":user.id,"alarmas":permisos.reportes_alarmas})
    else:
    	return Response({"status": False,"error": "E003","description":"USER INACTIVE"})


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def updateUser(request,pk):
	print "=======updateUser======="

	nombre = request.data.get("nombre")
	apellidoP = request.data.get("apellidoP")
	organizacion = request.data.get("organizacion")
	
	contrasenia = request.data.get("contrasenia",None)
	usuario = request.data.get("tipo_usuario")
	general_lectura = request.data.get("general_lectura",None)
	general_escritura = request.data.get("general_escritura",None)
	reportes_tickets = request.data.get("reportes_tickets",None)
	reportes_alarmas = request.data.get("reportes_alarmas",None)
	reportes_performance = request.data.get("reportes_performance",None)
	reportes_slas = request.data.get("reportes_slas",None)
	configuracion_usuarios = request.data.get("configuracion_usuarios",None)
	personalizacion_logotipo = request.data.get("personalizacion_logotipo",None)
	tickets_consultar = request.data.get("tickets_consultar",None)
	tickets_registrar = request.data.get("tickets_registrar",None)
	uuid = request.data.get("uuid",None)

	#Logic to Update the resource
	#GetPojosss
	try:
		user = User.objects.get(id=pk)
		perfil = PerfilUsuario.objects.get(user = user)
		org,detail = Organizacion.objects.get_or_create(descripcion = organizacion)
		permisos = Permiso.objects.get(user=user)
		#UpdatePojos
		user.first_name = nombre
		user.last_name = apellidoP

		if contrasenia != None:
			try:
				contrasenia = descryptPassword(contrasenia)
				print "DESCRYPT PASSWORD"
				print contrasenia
			except Exception as e:
				return Response({"status": False,"error": "U001","description":"OFFLINE MODULE"})

			if not check_password(contrasenia, user.password):
				print "Update Password"
				user.set_password(contrasenia)

		user.save()

		if (usuario):
			perfil.tipoUsuario = "2001"
		else:
			perfil.tipoUsuario = "2002"

		if (uuid != None):
			perfil.uuidDevice = uuid


		print "Kind Usuario"
		print perfil.tipoUsuario

		perfil.statusUser = 'S002'
		perfil.contadorIntents = 0
		perfil.organizacion = org
		perfil.save()
		if general_lectura != None:
			permisos.general_lectura = general_lectura
		if general_escritura != None:
			permisos.general_escritura = general_escritura
		if reportes_tickets != None:
			permisos.reportes_tickets = reportes_tickets
		if reportes_performance != None:
			permisos.reportes_performance = reportes_performance
		if reportes_slas != None:
			permisos.reportes_slas = reportes_slas
		if configuracion_usuarios != None:
			permisos.configuracion_usuarios = configuracion_usuarios
		if personalizacion_logotipo != None:
			permisos.personalizacion_logotipo = personalizacion_logotipo
		if tickets_consultar != None:
			permisos.tickets_consultar = tickets_consultar
		if tickets_registrar != None:
			permisos.tickets_registrar = tickets_registrar
		if reportes_alarmas != None:
			permisos.reportes_alarmas = reportes_alarmas


		print "Values of Permisos"
		print permisos.reportes_alarmas
		print perfil.tipoUsuario

		permisos.save()
		return Response({"status": True})

	except Exception, e:
		print e
		return Response({"status": False,"error": "U002","description":"UNKOWN ERROR"})

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def deleteUser(request,pk):
	try:
		user = User.objects.get(id=pk)
		perfil = PerfilUsuario.objects.get(user=user)
		perfil.statusUser = 'S003'
		perfil.save()

		user.is_active = False
		user.save()
		return Response({"result": True})
	except Exception, e:
		return Response({"result": False})

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def createUser(request):
	try:
		username = request.data.get("username")
		password = request.data.get("password")

		try:
			password = descryptPassword(password)
		except Exception as e:
			return Response({"status": False,"error": "C003","description":"ERROR MODULE OFFLINE"})

		nombreUsuario = request.data.get("nombreUsuario")
		apellidoUsuario = request.data.get("apellidoUsuario")


		organizacion = request.data.get("organizacion")
		tipoUsuario = request.data.get("tipoUsuario")
		alarmas = request.data.get("alarmas")
		uuid = request.data.get("uuid")

		user = User(username = username,last_name = apellidoUsuario , first_name = nombreUsuario)
		user.set_password(password)
		user.save()

		org,created = Organizacion.objects.get_or_create(descripcion = organizacion)

		perfil = PerfilUsuario(user = user, tipoUsuario = tipoUsuario,organizacion = org, uuidDevice=uuid)
		perfil.statusUser = 'S002'
		perfil.save()

		permisos = Permiso(user = user)
		permisos.reportes_alarmas = alarmas
		permisos.save()

		return Response({"status": True,"userId":user.id})
	except IntegrityError as e: 
		print e
		return Response({"status": False,"error": "C001","description":"ERROR INTEGRITY"})
	except Exception, e:
		return Response({"status": False,"error": "C002","description":"UNKOWN ERROR"})

class UserPerfilList(generics.ListCreateAPIView):
	model=PerfilUsuario
	serializer_class = UserPerfilUsuario
	permission_classes = (IsAuthenticated,)
	def get_queryset(self):
		u = self.request.user
		queryset = PerfilUsuario.objects.filter(~Q(user__id = u.id),user__is_active=True)
		return queryset

class UserPermisos(generics.RetrieveUpdateAPIView):
	model=Permiso
	permission_classes = (IsAuthenticated,)
	serializer_class = PermisosSerializer
	def get_queryset(self):
		queryset = Permiso.objects.all()
		return queryset

class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)