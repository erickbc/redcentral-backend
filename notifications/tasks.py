# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from estadisticas.views import tokenAppEnlace
from django.conf import settings
import requests
import json
from django.http import JsonResponse
from usuarios.models import *
from django.contrib.auth.models import User
from pyfcm import FCMNotification
from django.db.models import Q

from celery.utils.log import get_task_logger
from celery.schedules import crontab
from celery.decorators import periodic_task
logger=get_task_logger(__name__)


def sendPushNotification(message,registration_ids):
	logger.info("sendPushNotification")
	push_service = FCMNotification(api_key=settings.API_KEY_CLOUD_FIREBASE)
	message_title = "Red Central"
	result = push_service.notify_multiple_devices(registration_ids=registration_ids, message_title=message_title, message_body=message,sound="Default")

def manageNotification(message):
	logger.info("manageNotification")
	registration_ids = []
	profiles = PerfilUsuario.objects.filter(user__is_active=True).filter(~Q(tokenDevice=None))
	for profile in profiles:
		registration_ids.append(profile.tokenDevice)

	if registration_ids:
		sendPushNotification(message,registration_ids)


def checkDevices(json):
	logger.info("checkDevices")
	totalColectores = totalSwitches = totalOlts = totalRouter = 0
	
	totalColectores = int(json['colectores'][0]['fallaGestionColectores'])
	totalColectores += int(json['colectores'][0]['fallaOpticaColectores'])
	totalColectores += int(json['colectores'][0]['downColectores'])

	if totalColectores > 0:
		return True

	totalSwitches = int(json['switches'][0]['fallaGestionSwitch'])
	totalSwitches += int(json['switches'][0]['downSwitch'])
	totalSwitches += int(json['switches'][0]['fallaOpticaSwitch'])
	
	if totalSwitches > 0:
		return True
	
	totalOlts = int(json['olts'][0]['fallaGestionOlt'])
	totalOlts += int(json['olts'][0]['downOlt'])
	totalOlts += int(json['olts'][0]['fallaOpticaOlt'])

	if totalOlts > 0:
		return True

	totalRouter = int(json['router'][0]['fallaGestionRouter'])
	totalRouter += int(json['router'][0]['downRouter'])
	totalRouter += int(json['router'][0]['fallaOpticaRouter'])

	if totalRouter > 0:
		return True

	return False

def checkInfra(json):
	logger.info("checkInfra")
	baja = media = critica = 0
	
	baja = int(json['baja'][0]['shelter'])
	baja += int(json['baja'][0]['jaguar'])
	baja += int(json['baja'][0]['shelterLD'])
	baja += int(json['baja'][0]['delta'])

	if baja > 0:
		return True

	media = int(json['media'][0]['shelter'])
	media += int(json['media'][0]['jaguar'])
	media += int(json['media'][0]['shelterLD'])
	media += int(json['media'][0]['delta'])

	
	if media > 0:
		return True
	
	critica = int(json['critica'][0]['shelter'])
	critica += int(json['critica'][0]['jaguar'])
	critica += int(json['critica'][0]['shelterLD'])
	critica += int(json['critica'][0]['delta'])

	if critica > 0:
		return True

	return False


def constructorString(words):

	w = list(str(words))

	position = 0
	for i, c in enumerate(reversed(w)):
		print str(i) + c
		if c == ',':
			print "Valor Encontrado!!!"
			position = i
			break

	

	w[position+1] = ' y '
	return "".join(w)


def checkInfraDetalle(json):
	logger.info("checkInfra")
	words = []

	delta = jaguar = shelter = shelterLD = 0
	
	shelter = int(json['baja'][0]['shelter'])
	shelter += int(json['media'][0]['shelter'])
	shelter += int(json['critica'][0]['shelter'])

	print "===TOTAL SHELTER=="
	print shelter
	print "===TOTAL SHELTER=="

	if shelter > 0:
		words.append("Shelter")
		

	shelterLD = int(json['baja'][0]['shelterLD'])
	shelterLD += int(json['media'][0]['shelterLD'])
	shelterLD += int(json['critica'][0]['shelterLD'])
	print "===TOTAL SHELTER LD=="
	print shelterLD
	print "===TOTAL SHELTER LD=="
	if shelterLD > 0:
		words.append("Shelter LD")
		
	
	jaguar = int(json['baja'][0]['jaguar'])
	jaguar += int(json['media'][0]['jaguar'])
	jaguar += int(json['critica'][0]['jaguar'])
	print "===TOTAL JAGUAR=="
	print jaguar
	print "===TOTAL JAGUAR=="
	if jaguar > 0:
		words.append("Jaguar")
		
	
	delta = int(json['baja'][0]['delta'])
	delta += int(json['media'][0]['delta'])
	delta += int(json['critica'][0]['delta'])
	print "===TOTAL DELTA=="
	print delta
	print "===TOTAL DELTA=="
	if delta > 0:
		words.append("Delta")
	
	print "===WORDS===="
	print words

	if len(words)!=0:
		equipos = ",".join(str(x) for x in words)

		#if len(words)>1:
		#	equipos = constructorString(str(equipos))
		
		message = "Existen equipos: "+equipos+" alarmados, requieren de atención inmediata"
		return True,message
	else:
		return False,""


@periodic_task(
	run_every=(crontab(minute='*/10')),
	name="check_health_devices",
	ignore_result=True
)
def checkStatusDevices():
	logger.info("checkStatusDevices")
	token = tokenAppEnlace()
	querystring = {"access_token":token}
	response = requests.request("POST", settings.URL_LISTA_TOTAL_INFRA, params=querystring)
	result =  json.loads(response.text)
	status,message = checkInfraDetalle(result)

	if status:
		manageNotification(message)



