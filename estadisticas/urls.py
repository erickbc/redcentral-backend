
from django.conf.urls import url,include
from django.contrib import admin

from estadisticas.views import *

urlpatterns = [
	url(r'^devices', information_estadisticas),
	url(r'^saludAlarmas', information_saludAlarmas),
	
	url(r'^ticketsGeneral', information_tickets),
	url(r'^noc/informationGeneral', information_noc_general),
	url(r'^care/informationGeneral', information_care_general),
	url(r'^care/detalle', information_care_detalle),
	url(r'^noc/detalle/back', information_noc_back),
	url(r'^noc/detalle/front', information_noc_front),

	url(r'^nocCare/detalle', information_nocCare),

	url(r'^listaAlarmas/router', listaAlarmas_router),
	url(r'^listaAlarmas/switchs', listaAlarmas_switchs),
	url(r'^listaAlarmas/colectores', listaAlarmas_colectores),
	url(r'^listaAlarmas/olt', listaAlarmas_olt),

	url(r'^infraestructura/totales', infra_totales),
	url(r'^infraestructura/detalles', infra_detalles),
	url(r'^infraestructura/equipos', information_infraestructuraTotal),
	url(r'^infraestructura/alarmas', information_infraestructuraAlarmas)

]
