# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


from django.http import JsonResponse
import json
from pprint import pprint
import requests

from django.conf import settings


def tokenAppEnlace():
	print "==========TOKEN APPENLANCE=========="
	querystring = {"grant_type":"password","username":"appnoc","password":"appnoc_2018"}
	headers = {
	'authorization': "Basic Y2xpZW50OmNsaWVudA==",
	}
	response = requests.request("POST", settings.URL_LOGIN, headers=headers, params=querystring)
	d = json.loads(response.text)
	return d["access_token"]

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def information_estadisticas(request):
	print "==========INFORMATION ESTADISTICAS=========="
	bandera = True
	if bandera:
		token = tokenAppEnlace()
		querystring = {"access_token":token}
		response = requests.request("POST", settings.URL_DEVICES, params=querystring)
		d = json.loads(response.text)
		return Response(d)
		#return JsonResponse(d,safe=True)
	else:
		data = json.load(open('mock.json'))
		return Response(d)

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def information_saludAlarmas(request):
	print "==========INFORMATION ESTADISTICAS=========="
	bandera = True
	if bandera:
		token = tokenAppEnlace()
		querystring = {"access_token":token}

		bodyRequest = json.loads(request.body)

		equipo = bodyRequest["tipoEquipo"].upper()
		alarma = bodyRequest["tipoAlarma"].upper()
		body={"tipoEquipo":"","tipoAlarma":""}

		#M.-Media
		#A.-Alta
		#C.-Critica
		switcherEquipo = {
			'R': "ROUTER",
			'S': "SWITCH",
			'C': "COLECTORES",
			'O': "OLTS",
		}

		body["tipoEquipo"] = switcherEquipo[(equipo)]


		switcherAlarma = {
			'G': "GESTION",
			'D': "DOWN",
			'O':"OPTICOS",
			'V':"VOLTAJE",
			'T':"TEMPERATURA",
			'M':"MEMORIA"

		}
		body["tipoAlarma"] = switcherAlarma[(alarma)]


		response = requests.request("POST", settings.URL_DEVICES_SALUD,json=body, params=querystring)
		d = json.loads(response.text)
		print d["lista"]
		return JsonResponse(d["lista"],safe=False)
	else:
		data = json.load(open('mock.json'))
		return JsonResponse(data)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def information_tickets(request):
	d={}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_TICKETS_CARE, params={"access_token":token})
	d["care"] = json.loads(response.text)

	response = requests.request("POST", settings.URL_TICKETS_NOC,  params={"access_token":token})
	d["noc"] = json.loads(response.text)
	return JsonResponse(d,safe=True)


@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def information_noc_general(request):
	d={}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_BACK_FRONT_NOC_GENERALES, params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d,safe=True)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def information_care_general(request):
	d={}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_BACK_FRONT_CARE_GENERALES, params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d,safe=True)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def information_care_detalle(request):
	d={}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_BACK_FRONT_CARE_DETALLE, params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d,safe=True)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def information_noc_back(request):
	d={}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_BACK_FRONT_NOC_DETALLE_BACK, params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d,safe=True)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def information_noc_front(request):
	d={}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_BACK_FRONT_NOC_DETALLE_FRONT, params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d,safe=True)

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def information_nocCare(request):
	print "===============information_nocCare=============="

	bodyRequest = json.loads(request.body)

	categoria = bodyRequest["categoria"]
	prioridad = bodyRequest["prioridad"]
	area = bodyRequest["area"].strip(' ').upper()[0]
	estado = bodyRequest["estado"]
	tipo = bodyRequest["tipo"]

	body={"categoria":"","prioridad":"","area":"","estado":""}
	
	body["categoria"] = categoria
	

	#M.-Media
	#A.-Alta
	#C.-Critica
	switcherPrioridad = {
		'M': "MEDIA",
		'A': "ALTA",
		'C': "CRITICA",
	}
	body["prioridad"] = switcherPrioridad[(prioridad)]

	# Area
	# B = BO
	# F = FO
	switcherArea = {
		'B': "BO",
		'F': "FO",
		'G':"GOBIERNO",
		'E':"ESTRATEGICOS",
		'M':"MONITOREO"
	}
	body["area"] = switcherArea[(area)]


	#N.-noc
	#C.-care
	switcherTipo = {
		'N': "noc",
		'C': "care",
	}
	body["tipo"] = switcherTipo[(tipo)]



	# Estado
	# 1 = Vencer (x Vencer)
	# 2 = Vencidos (Vencidos)
	# 3 = Abiertos (Abiertos)


	switcherEstado = {
		'1': "vencer",
		'2': "vencidos",
		'3': "abiertos",
	}
	body["estado"] = switcherEstado[(estado)]

	

	print "============BODY CALL SERVICE==========="
	print body
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_TICKETS_NOC_CARE, json=body,params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d,safe=True)


	#print json.loads(request.body)["categoria"]

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def information_infraestructuraTotal(request):
	print "===============information_infraestructuraTotal=============="
	print json.loads(request.body)
	bodyRequest = json.loads(request.body)
	dispositivo = bodyRequest["dispositivo"]
	if(dispositivo == "GABINETES CORE"):
		dispositivo = "GABINETE CORE"
	body={"dispositivo":dispositivo}



	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_LISTA_TOTAL_DETALLES_EQUIPO, json=body,params={"access_token":token})
	
	# GET ALARMAS =====
	responseA = requests.request("POST", settings.URL_LISTA_TOTAL_DETALLE_ALARMA, json=body,params={"access_token":token})

	# GET ALARMAS =====
	responseA = json.loads(responseA.text)
	responseD = json.loads(response.text)
	listaAlarmas = responseA['listAlarmas']
	for equipo in responseD['listEquipos']:
		index = 0
		equipo["alarma"] = False
		find = False
		for alarma in listaAlarmas:
			if(equipo['modelName']==alarma['modelName']):
				equipo["alarma"] = True
				equipo["tituloAlarma"] = alarma['tituloAlarma']
				equipo["level"] = alarma['severity']
				equipo["fechaAlarma"] = alarma['fechaAlarma']
				equipo["numeroTicket"] = alarma['numeroTicket']
				equipo["color"] = alarma['color']
				find = True
				break
			index += 1
		if find:
			listaAlarmas.pop(index)

	return JsonResponse(responseD,safe=True)

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def information_infraestructuraAlarmas(request):
	print "===============information_infraestructuraAlarmas=============="
	bodyRequest = json.loads(request.body)
	dispositivo = bodyRequest["dispositivo"]
	if(dispositivo == "GABINETES CORE"):
		dispositivo = "GABINETE CORE"
	body={"dispositivo":dispositivo}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_LISTA_TOTAL_DETALLE_ALARMA, json=body,params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d,safe=True)




@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def listaAlarmas_router(request):
	d={}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_LISTA_ALARMAS, params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d["router"],safe=False)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def listaAlarmas_switchs(request):
	d={}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_LISTA_ALARMAS, params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d["switchs"],safe=False)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def listaAlarmas_colectores(request):
	d={}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_LISTA_ALARMAS, params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d["colectores"],safe=False)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def listaAlarmas_olt(request):
	d={}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_LISTA_ALARMAS, params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d["olt"],safe=False)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def infra_detalles(request):
	d={}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_LISTA_DETALLE_INFRA, params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d,safe=True)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def infra_totales(request):
	d={}
	token = tokenAppEnlace()
	response = requests.request("POST", settings.URL_LISTA_TOTAL_INFRA, params={"access_token":token})
	d = json.loads(response.text)
	return JsonResponse(d,safe=True)

