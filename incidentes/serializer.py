from rest_framework import serializers
from incidentes.models import *


class SolicitanteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Solicitante
        fields = ('id','descripcion')


class CategoriaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Categoria
        fields = ('id','descripcion')

class SubCategoriaSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubCategoria
        fields = ('id','descripcion')

class FallaIncidentesSerializer(serializers.ModelSerializer):

    class Meta:
        model = FallaIncidentes
        fields = ('id','descripcion')


class IncidenteSerializer(serializers.ModelSerializer):
    solicitante = SolicitanteSerializer(read_only=True)
    categoria = CategoriaSerializer(read_only=True)
    subcategoria = SubCategoriaSerializer(read_only=True)
    fallaIncidente = FallaIncidentesSerializer(read_only=True)
    justificacion = serializers.CharField(allow_blank=True)
    """
    solicitante = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='descripcion'
     )
    categoria = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='descripcion'
     )
    subcategoria = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='descripcion'
     )
    fallaIncidente = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='descripcion'
     )
    """
    class Meta:
        model = Incidente
        fields = ( 'id','solicitante','categoria','subcategoria','fallaIncidente','descripcion','status','justificacion')

