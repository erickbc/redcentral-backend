# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from incidentes.models import Solicitante

admin.site.register(Solicitante)
# Register your models here.
