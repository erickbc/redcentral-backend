# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from incidentes.models import *
from incidentes.serializer import *
from rest_framework import generics
from rest_framework.decorators import api_view
# Create your views here.
class ListaIncidentes(generics.ListCreateAPIView):
	model=Incidente
	serializer_class = IncidenteSerializer

	def get_queryset(self):
		queryset = Incidente.objects.all()
		return queryset
	def perform_create(self, serializer):
		cat = self.request.data.get('categoria')
		fal = self.request.data.get('fallaIncidente')
		sol = self.request.data.get('solicitante')
		subCat = self.request.data.get('subcategoria')

		falla = FallaIncidentes.objects.get(id=fal)
		categoria = Categoria.objects.get(id=cat)
		solicitante = Solicitante.objects.get(id=sol)
		subCategoria = SubCategoria.objects.get(id=subCat)

		serializer.save(categoria=categoria,fallaIncidente=falla,solicitante=solicitante,subcategoria=subCategoria)


class ListaCategorias(generics.ListCreateAPIView):
	model=Categoria
	serializer_class = CategoriaSerializer

	def get_queryset(self):
		queryset = Categoria.objects.all()
		return queryset

class ListaSubCategorias(generics.ListCreateAPIView):
	model=SubCategoria
	serializer_class = SubCategoriaSerializer

	def get_queryset(self):
		idCategoria = self.kwargs["idCategoria"]
		queryset = Categoria.objects.get(id=idCategoria).subcategorias.all()
		#queryset = SubCategoria.objects.filter(categoria = categoria)
		return queryset

@api_view(["POST"])
def updateIncidente(request,pk):
	print "=======updateUser======="
	status = request.data.get("status",None)
	justificacion = request.data.get("justificacion",None)
	#Logic to Update the resource
	#GetPojosss
	incidente = Incidente.objects.get(id=pk)
	try:
		
		if status != None:
			incidente.status = status
		if justificacion != None:
			incidente.justificacion = justificacion

		incidente.save()
		return Response({"result": True})

	except Exception, e:
		return Response({"result": False})




