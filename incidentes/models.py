# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import datetime
# Create your models here.

class SubCategoria(models.Model):
	descripcion = models.CharField(max_length=30, unique = True)
	status = models.BooleanField(default=True)

class Categoria(models.Model):
	descripcion = models.CharField(max_length=30, unique = True)
	status = models.BooleanField(default=True)
	subcategorias = models.ManyToManyField(SubCategoria,related_name='subcategoria')


class Solicitante(models.Model):
	descripcion = models.CharField(max_length=30, unique = True)
	status = models.BooleanField(default=True)
	photo=models.ImageField(upload_to='photo_solicitante', verbose_name='Imagen',null=True)


class FallaIncidentes(models.Model):
	descripcion = models.CharField(max_length=30, unique = True)
	status = models.BooleanField(default=True)

class Incidente(models.Model):
	TYPE_STATUS=(
		('01_TO','TODOS'),
		('02_EC','EN CURSO'),
		('03_CE','CERRADO'),
		('04_RE','RESUELTO'),
	)
	solicitante = models.ForeignKey(Solicitante)
	categoria = models.ForeignKey(Categoria)
	subcategoria = models.ForeignKey(SubCategoria)
	fallaIncidente = models.ForeignKey(FallaIncidentes)
	descripcion = models.CharField(max_length=250)
	status = models.CharField(max_length=4,choices=TYPE_STATUS)
	justificacion = models.CharField(max_length=250,blank=True, null=True)
	fechaApertura= models.DateTimeField( auto_now_add=True)
	fechaModificacion= models.DateTimeField( auto_now_add=True,blank=True, null=True)



