
from django.conf.urls import url,include
from django.contrib import admin

from incidentes.views import *

urlpatterns = [
	url(r'^$', ListaIncidentes.as_view()),
	url(r'^categorias/$', ListaCategorias.as_view()),
	url(r'^subcategoria/(?P<idCategoria>[0-9]+)/$', ListaSubCategorias.as_view()),
	url(r'^update/(?P<pk>[0-9]+)/$', updateIncidente,name='update-incidente'),
	#url(r'^(?P<pk>[0-9]+)/$', UserDetail.as_view(),name='user-detail'),
	#url(r'^delete/(?P<pk>[0-9]+)/$', deleteUser,name='user-delete'),
	#url(r'^permisos/(?P<pk>[0-9]+)/$', UserPermisos.as_view(),name='delete-user'),
	#url(r'^update/(?P<pk>[0-9]+)/$', updateUser,name='user-permiso'),
]


