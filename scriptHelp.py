from usuarios.models import *
"""
categoria1 = Categoria(descripcion="General",status=False)
categoria1.save()

categoria2 = Categoria(descripcion="Reportes",status=False)
categoria2.save()

categoria3 = Categoria(descripcion="Configuracion",status=False)
categoria3.save()

categoria4 = Categoria(descripcion="Tickets",status=False)
categoria4.save()

categoria5 = Categoria(descripcion="Personalizacion",status=False)
categoria5.save()


from django.contrib.auth.models import User

permisoEscritura = Permiso(descripcion = "Escritura",status=False,categoria=Categoria.objects.get(descripcion="General"))
permisoEscritura.save()

permisoLectura = Permiso(descripcion = "Lectura",status=False,categoria=Categoria.objects.get(descripcion="General"))
permisoLectura.save()


permisoTickets = Permiso(descripcion = "Tickets",status=False,categoria=Categoria.objects.get(descripcion="Reportes"))
permisoTickets.save()

permisoPerformance = Permiso(descripcion = "Performance",status=False,categoria=Categoria.objects.get(descripcion="Reportes"))
permisoPerformance.save()

permisoSLAs = Permiso(descripcion = "SLAs",status=False,categoria=Categoria.objects.get(descripcion="Reportes"))
permisoSLAs.save()


permisoUsuarios = Permiso(descripcion = "Usuarios",status=False,categoria=Categoria.objects.get(descripcion="Configuracion"))
permisoUsuarios.save()

permisoConsultar = Permiso(descripcion = "Consultar",status=False,categoria=Categoria.objects.get(descripcion="Tickets"))
permisoConsultar.save()

permisoRegistrar = Permiso(descripcion = "Registrar",status=False,categoria=Categoria.objects.get(descripcion="Tickets"))
permisoRegistrar.save()


permisoLogotipo = Permiso(descripcion = "Logotipo",status=False,categoria=Categoria.objects.get(descripcion="Personalizacion"))
permisoLogotipo.save()

"""
from usuarios.models import *
from incidentes.models import *
from django.contrib.auth.models import User

for user in User.objects.all():
	userPerm,yet = Permiso.objects.get_or_create(user = user)



oganizacion1 = Organizacion(descripcion = "Banamex",status = True)
oganizacion1.save()
oganizacion2 = Organizacion(descripcion = "Izzi",status = True)
oganizacion2.save()

oganizacion3 = Organizacion(descripcion = "Bimbo",status = True)
oganizacion3.save()

for user in User.objects.all():
	perfil = PerfilUsuario(user=user,tipoUsuario="Usuario",organizacion=oganizacion1)
	perfil.save()

subCat1 = SubCategoria(descripcion="Enlaces Dedicados")
subCat2 = SubCategoria(descripcion="Internet")
subCat3 = SubCategoria(descripcion="Lan to Lan")
subCat4 = SubCategoria(descripcion="Configuracion Bam")
subCat5 = SubCategoria(descripcion="Red Local")
subCat1.save()
subCat2.save()
subCat3.save()
subCat4.save()
subCat5.save()


Cat1 = Categoria(descripcion="Servicio Estrategico")
Cat2 = Categoria(descripcion="Servicio Telefonia")
Cat3 = Categoria(descripcion="Servicio Terceros")
Cat4 = Categoria(descripcion="Servicio Redes")
Cat5 = Categoria(descripcion="Servicio Mantenimiento")
Cat1.save()
Cat2.save()
Cat3.save()
Cat4.save()
Cat5.save()


Cat1.subcategorias.add(subCat1)
Cat1.subcategorias.add(subCat2)

Cat2.subcategorias.add(subCat1)
Cat2.subcategorias.add(subCat2)
Cat2.subcategorias.add(subCat3)

Cat3.subcategorias.add(subCat1)
Cat3.subcategorias.add(subCat2)
Cat3.subcategorias.add(subCat3)


Cat4.subcategorias.add(subCat3)
Cat4.subcategorias.add(subCat4)

Cat5.subcategorias.add(subCat4)
Cat5.subcategorias.add(subCat5)

Cat1.save()
Cat2.save()
Cat3.save()
Cat4.save()
Cat5.save()

sol1 = Solicitante(descripcion="Seolicitante 1")
sol2 = Solicitante(descripcion="Seolicitante 2")
sol3 = Solicitante(descripcion="Seolicitante 3")
sol4 = Solicitante(descripcion="Seolicitante 4")
sol5 = Solicitante(descripcion="Seolicitante 5")

sol1.save()
sol2.save()
sol3.save()
sol4.save()
sol5.save()



falla1 = FallaIncidentes(descripcion="Bajo Ancho Banda")
falla2 = FallaIncidentes(descripcion="Falla Total")
falla3 = FallaIncidentes(descripcion="Intermitencia")
falla4 = FallaIncidentes(descripcion="Conexion Red")
falla5 = FallaIncidentes(descripcion="Falla Parcial")

falla1.save()
falla2.save()
falla3.save()
falla4.save()
falla5.save()


inc1 = Incidente(solicitante = sol1,categoria=Cat1,subcategoria = subCat1,fallaIncidente=falla1,descripcion="Descripcion1",status = '01_TO')
inc1.save()
inc2 = Incidente(solicitante = sol1,categoria=Cat1,subcategoria = subCat2,fallaIncidente=falla2,descripcion="Descripcion2",status = '02_EC')
inc2.save()
inc3 = Incidente(solicitante = sol2,categoria=Cat2,subcategoria = subCat1,fallaIncidente=falla3,descripcion="Descripcion3",status = '03_CE')
inc3.save()
inc4 = Incidente(solicitante = sol2,categoria=Cat2,subcategoria = subCat3,fallaIncidente=falla4,descripcion="Descripcion4",status = '04_RE')
inc4.save()
inc5 = Incidente(solicitante = sol3,categoria=Cat3,subcategoria = subCat1,fallaIncidente=falla5,descripcion="Descripcion5",status = '01_TO')
inc5.save()
inc6 = Incidente(solicitante = sol4,categoria=Cat4,subcategoria = subCat4,fallaIncidente=falla1,descripcion="Descripcion6",status = '02_EC')
inc6.save()
inc7 = Incidente(solicitante = sol5,categoria=Cat5,subcategoria = subCat5,fallaIncidente=falla2,descripcion="Descripcion7",status = '03_CE')
inc7.save()


	
